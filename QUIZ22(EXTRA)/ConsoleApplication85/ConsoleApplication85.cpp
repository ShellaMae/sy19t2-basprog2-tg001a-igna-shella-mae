// ConsoleApplication85.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#ifndef NODE_H
#define NODE_H
#include <iostream>
#include <conio.h>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

struct Node {
	string name;
	Node* previous;
	Node* next;
};
#endif //NODE_H
int main()
{
	int total = 5;
	int count;
	Node* player1 = new Node;
	Node* player2 = new Node;
	Node* player3 = new Node;
	Node* player4 = new Node;
	Node* player5 = new Node;

	cout << "What's your name soldier?" << endl;
	cin >> player1->name;

	cin >> player2->name;
	player1->next = player2;
	player2->previous = player1;

	cin >> player3->name;
	player2->next = player3;
	player3->previous = player2;

	cin >> player4->name;
	player3->next = player4;
	player4->previous = player3;

	cin >> player5->name;
	player4->next = player5;
	player5->previous = player4;

	player5->next = player1;
	player1->previous = player5;

	Node* current = player1;
	Node* tobeDeleted;

	srand(time(0));

	system("cls");

	cout << player1->name << " is the start of the elimination " << endl << endl;

	while (total > 0)
	{
		count = rand() % total + 1;

		for (int i = 0; i < count; i++)
		{
			current = current->next;
		}

		tobeDeleted = current;

		cout << "The count has counted " << count << " player targeted " << current->name << " the previous " << current->previous->name << " the next " << current->next->name << endl << endl;


		current->next->previous = current->previous;
		current->previous->next = current->next;
		current = current->next;

		cout << "The next player " << current->name << " his previous " << current->previous->name << " his next " << current->next->name << endl << endl;

		cout << tobeDeleted->name << " was eliminated." << endl;

		if (total == 1)
		{
			cout << current->name << " will go to seek for reinforcements." << endl;
		}

		delete tobeDeleted;

		total--;
		system("pause");
	}

}