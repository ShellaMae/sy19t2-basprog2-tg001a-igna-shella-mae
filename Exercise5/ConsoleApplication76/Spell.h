#pragma once
#include <string>
#include "Wizard.h"

using namespace std;

class Spell
{
public:
	Spell(string name, int damage, int mpCost);
	~Spell();

	string getName();
	int getDamage();
	int getMpCost();
	
	void use(Wizard* action, Wizard* target);

private:
	string mName;
	int mDamage;
	int mMpCost;
};

