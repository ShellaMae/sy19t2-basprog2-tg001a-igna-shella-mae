#pragma once
#include <string>
#include <iostream>
#include "Spell.h"

using namespace std;

class Spell;

class Wizard
{
public:
	Wizard();
	Wizard(string namr, int hp, int mp, Spell*spell);
	~Wizard();

	bool castSpell(Wizard*target);
	void viewStats();

	int getHp();
	int getMp();
	string getName();
	Spell*getSpell();

	void takeDamage(int damage);

private:
	string mName;
	int mHitPoints;
	int mMagicPoints;
	Spell*mSpell;
};

