#include "Spell.h"

Spell::Spell(string name, int damage, int mpCost)
{
	mName = name;
	mDamage = damage;
	mMpCost = mpCost;
}

Spell::~Spell()
{
}

string Spell::getName()
{
	return mName;
}

int Spell::getDamage()
{
	return mDamage;
}

int Spell::getMpCost()
{
	return mMpCost;
}

void Spell::use(Wizard * action, Wizard * target)
{
	cout << action->getName() << " is casting " << mName << " on " << target->getName() << endl;
	target->takeDamage(mDamage);
	cout << target->getName() << " received " << mDamage << " damage" << endl;
}

