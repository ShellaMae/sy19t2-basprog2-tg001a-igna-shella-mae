#include "Wizard.h"

Wizard::Wizard()
{
	mName = "Some Wizard";
	mHitPoints = 100;
	mMagicPoints = 50;

	mSpell = new Spell("Fireball", 20, 10);
}

Wizard::Wizard(string name, int hp, int mp, Spell* spell)
{
	mName = name;
	mHitPoints = hp;
	mMagicPoints = mp;

	mSpell = spell;
}

bool Wizard::castSpell(Wizard* target)
{
	
	if (mSpell->getMpCost() > mMagicPoints)
	{
		cout << mName << " doesn't have enough MP" << endl;
		return false;
	}

	mSpell->use(this, target);
	return true;
}

void Wizard::viewStats()
{
	cout << "Name = " << mName << endl;
	cout << "HP = " << mHitPoints << endl;
	cout << "MP = " << mMagicPoints << endl;
}

string Wizard::getName()
{
	return "Hello, my name is " + mName;
}

Spell * Wizard::getSpell()
{
	return mSpell;
}

int Wizard::getHp()
{
	return mHitPoints;
}

int Wizard::getMp()
{
	return mMagicPoints;
}

void Wizard::takeDamage(int damage)
{
	if (damage < 0) return;

	mHitPoints -= damage;
	if (mHitPoints < 0) mHitPoints = 0;
}


Wizard::~Wizard()
{
	cout << "Wizard: " << this << " is about to be deleted" << endl;
}

