// ConsoleApplication76.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include "Wizard.h"

using namespace std;

int main()
{
	Wizard*wizard1 = new Wizard();

	string name;
	cout << "Input name: ";
	cin >> name;

	int hp;
	cout << "Input HP: ";
	cin >> hp;

	int mp;
	cout << "Input MP: ";
	cin >> mp;

	int xp;
	cout << "Input XP: ";
	cin >> xp;

	Spell*thunderShock = new Spell("Thundershock", 15, 15);
	Wizard*wizard2 = new Wizard(name, hp, mp, thunderShock);

	cout << wizard2->getHp() << endl;

	while (wizard1->getHp() > 0 || wizard2->getHp() > 0 || wizard1->getMp() > 0 || wizard2->getMp() > 0)
	{
		wizard1->castSpell(wizard2);
		wizard2->castSpell(wizard1);
	}

	system("pause");
	delete wizard1;
	delete wizard2;

	return 0;
}