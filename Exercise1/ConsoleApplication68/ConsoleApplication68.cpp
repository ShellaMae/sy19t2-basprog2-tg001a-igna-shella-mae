// ConsoleApplication68.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct Item
{
	int goldValue;
	string name;
};

Item randomItem() 
{
	Item*items = new Item[20];
	items[0].name = "Red Potion";
	items[0].goldValue = 10;
	items[1].name = "Blue Potion";
	items[1].goldValue = 20;
	items[2].name = "YggdrasilLeaf";
	items[2].goldValue = 30;
	items[3].name = "Elixir";
	items[3].goldValue = 50;
	items[4].name = "TeleportScroll";
	items[4].goldValue = 60;
	items[5].name = "AetherWisp";
	items[5].goldValue = 80;
	items[6].name = "ArdentCenser";
	items[6].goldValue = 90;
	items[7].name = "BlastingWand";
	items[7].goldValue = 80;
	items[8].name = "BloodThirster";
	items[8].goldValue = 90;
	items[9].name = "BlackSpear";
	items[9].goldValue = 90;
	items[10].name = "DarkSeal";
	items[10].goldValue = 70;
	items[11].name = "ControlWand";
	items[11].goldValue = 10;
	items[12].name = "DoranBlade";
	items[12].goldValue = 20;
	items[13].name = "FrozenMallet";
	items[13].goldValue = 30;
	items[14].name = "FrozenFist";
	items[14].goldValue = 50;
	items[15].name = "Hexdrinker";
	items[15].goldValue = 60;
	items[16].name = "HarrowingCrescent";
	items[16].goldValue = 80;
	items[17].name = "FrozenHeart";
	items[17].goldValue = 90;
	items[18].name = "FiendishCodex";
	items[18].goldValue = 80;
	items[19].name = "GravityBoots";
	items[19].goldValue = 90;

	int totalGold;

	for ( int i = 0; i <= 9;i++)
	{
		cout << i+1<<"."<< "Random Items: " << items[i].name << " with value " << items[i].goldValue << endl;
		totalGold = items[i].goldValue + items[i].goldValue;
		system("pause");
		cout << endl;
	}
	cout << "The total amount of the gold is: " << totalGold << endl;
	cout << endl;
	system("pause");

	return randomItem();
}

int main()
{
	srand(time(NULL));
	randomItem();

	return 0;
}