// ConsoleApplication72.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <string>
#include <iostream>
#include <time.h>
#include <vector>

using namespace std;

struct Item
{
	string name;
	int item;
};

Item* randomItem()
{
	int x = rand() % 5;

	Item*items = new Item[5];

	switch (x)
	{
	case 0:
		items->name = "Mithril Ore";
		items->item = 100;

		break;
	case 1:
		items->name = "Sharp Talon";
		items->item = 50;
		break;
	case 2:
		items->name = "Thick Leather";
		items->item = 25;
		break;
	case 3:
		items->name = "Jellopy";
		items->item = 5;
		break;
	case 4:
		items->name = "Cursed Stone";
		items->item = 0;
		break;
	}



	return items;
}

void enterDungeon(int&gold, int fee, int&tempGold)
{
	cout << "Welcom to the Dungeon! That will cost you 25 gold." << endl;
	system("pause");
	cout << endl;

	gold -= fee;
	int multiplier=1;
	char choice;
	bool enterDungeon = true;
	
	while (enterDungeon = true) 
	{
		vector<Item*>items;
		Item*Loot = randomItem();
		items.push_back(Loot);

		tempGold = tempGold + (Loot->item*multiplier);

		if (Loot->item == 0)
		{
			cout << "You have encountered a " << Loot->name << " and have fainted." << endl;
			system("pause");
			system("cls");
			break;
		}

		else if (Loot->item != 0) 
		{
			cout << "would you like to continue exploring?? y/n";
			cin >> choice;

			if (choice == 'y') 
			{
				if (multiplier < 4)
				{
					multiplier++;
				}
			}

			else if (choice == 'n')
			{
				cout << "Thank you for coming." << endl;
				system("pause");
				system("cls");
				gold = gold + tempGold;
				break;
			}
			system("pause");
			system("cls");
		}
	}

	if (gold < 500)
	{
		vector<Item*>items;
		for (int i = 0; i < items.size(); i++)
		{
			cout << "Looted Item:" << endl;
			cout << i + 1 << ". " << items[i];
		}
		cout << "Current Gold: " << gold << endl;
		cout << "Defeat." << endl;
	}
	else if (gold >= 500)
	{
		vector<Item*>items;
		for (int i = 0; i < items.size(); i++)
		{
			cout << "Looted Item:" << endl;
			cout << i + 1 << ". " << items[i];
		}
		cout << "Current Gold: " << gold << endl;
		cout << "VICTORY!" << endl;
	}

}

int main()
{
	srand(time(NULL));

	int enterGold = 50;
	int tempGold = 0;

	enterDungeon(enterGold, 25, tempGold);
	system("pause");
	return 0;
}

