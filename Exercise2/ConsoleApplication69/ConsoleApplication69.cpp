// ConsoleApplication69.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct Loot
{
	int gold;
	string name;
};

Loot*randomItem() 
{
	Loot*Items = new Loot[5];
	Items[0].name = "Mithril Ore";
	Items[0].gold = 100;
	Items[1].name = "Sharp Talon";
	Items[1].gold = 50;
	Items[2].name = "Thick Leather";
	Items[2].gold = 25;
	Items[3].name = "Jellopy";
	Items[3].gold = 5;
	Items[4].name = "cursed stone";
	Items[4].gold = 0;

	int x = rand() % 5;
	cout << "You found a " << Items[x].name << " with value " << Items[x].gold << endl;
	return &Items[x];

}

int enterDungeon(int& goldValue,int fee,int &tempGold) 
{
	Loot*Items = new Loot;
	cout << "Welcom to the Dungeon! That will cost you 25 gold." << endl;
	system("pause");
	cout << endl;

	goldValue -= fee;
	char choice;
	bool enterDungeon=true;
	int multiply = 1;
	string lootItems;

	while (enterDungeon = true)
	{
		Items = randomItem();
		tempGold = tempGold + (Items->gold*multiply);
		
		if (Items->gold == 0)
		{
			cout << "You have found a " << Items->name << " and have died." << endl;
			lootItems = Items->name;
			system("pause");
			cout << endl;
			break;
		}

		else if (Items->gold != 0)
		{
			cout << "Would you like to continue exploring? y/n:";
			cin >> choice;
			cout << endl;
			
			if (choice == 'y') {
				if (multiply < 4)
				{
					multiply++;
				}
			}

			if (choice == 'Y') {
				if (multiply < 4)
				{
					multiply++;
				}
			}

			else if (choice == 'n') {
				cout << "Thank you for coming!" << endl;
				system("pause");
				goldValue = goldValue + tempGold;
				break;
			}

			else if (choice == 'N') {
				cout << "Thank you for coming!" << endl;
				system("pause");
				goldValue = goldValue + tempGold;
				break;
			}
			  system("pause");
			
		}
	}

	if (goldValue < 500)
	{
		cout << "Current Gold: " << goldValue << endl;
		cout << "Item that you LOOT!: " << lootItems << endl;
		system("pause");
		cout << "You Lose!" << endl;
	}

	else if (goldValue >= 500)
	{
		int totalGold=0;
		cout << "Current Gold: " << goldValue << endl;
		system("pause");
		cout << endl;
		cout << "You Win!" << endl;
	}

	return goldValue;
}

int main()
{
	srand(time(NULL));

	int enterGold = 50;
	int tempMoney = 0;

	enterDungeon(enterGold, 25, tempMoney);
	system("pause");

	return 0;
}
