// ConsoleApplication75.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <conio.h>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

//printing card
struct aiECards {
	string card;
};

struct aiSCards {
	string cards;
};

aiECards*randomCards() {

	aiECards*eCards = new aiECards[5];
	eCards[0].card = "Emperor";
	eCards[1].card = "Civillian";
	eCards[2].card = "Civillian";
	eCards[3].card = "Civillian";
	eCards[4].card = "Civillian";


	int x = rand() % 5 + 1;
	cout << eCards[x].card;

	return &eCards[x];
}

aiSCards*randCards() {

	aiSCards*sCards = new aiSCards[5];
	sCards[0].cards = "Slave";
	sCards[1].cards = "Civillian";
	sCards[2].cards = "Civillian";
	sCards[3].cards = "Civillian";
	sCards[4].cards = "Civillian";


	int y = rand() % 5 + 1;
	sCards[0].cards = sCards[y].cards;

	return &sCards[y];
}

void printEmperor(vector<string>&emperorCards, vector<string>&slaveCards) {

	int playCard, cash = 0, mm,round=1;
	aiSCards*sCards = new aiSCards;
	sCards = randCards();
	aiECards*eCards = new aiECards;
	eCards = randomCards();
	bool playRound = true;

	emperorCards.push_back("Emperor");
	emperorCards.push_back("Civillian");
	emperorCards.push_back("Civillian");
	emperorCards.push_back("Civillian");
	emperorCards.push_back("Civillian");

	while (playRound == true) 
	{
		cout << "Cash: " << cash << endl;
		cout << "Distance left(mm): 30" << endl;
		cout << "Round= "<<round<<"/12"<< endl;
		round++;
		cout << "Side:Emperor" << endl;
		cout << endl;
		cout << "How many mm would you like to wager, Kaiji?" << endl;
		cin >> mm;

		if (mm <= 30) 
		{
			cout << "Pick a card to play..." << endl;
			cout << "======================" << endl;
			for (unsigned int i = 0; i < emperorCards.size(); i++) {
				cout << "[" << i + 1 << "] " << emperorCards[i] << endl;
			}
			cin >> playCard;

			slaveCards.push_back("Slave");
			slaveCards.push_back("Civillian");
			slaveCards.push_back("Civillian");
			slaveCards.push_back("Civillian");
			slaveCards.push_back("Civillian");

			if (playCard == 1) 
			{
				if (sCards->cards == "Slave") 
				{
					cout << "You lost! The pin will now move by 30mm" << endl;
					cout << "Drill....drill....drill...drill..." << endl;
					cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
					system("pause");
					cout << "The pin has pierced your eardrum." << endl;
					cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
					cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
					system("pause");
					system("cls");
					break;
				}
				else if (sCards->cards == "Civillian") 
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Emperor vs [Tonegawa] Citizen" << endl;
					cout << endl;
					mm = mm * 100000;
					cout << "You won " << mm << " yen" << endl;
					cash = mm+mm;
					system("pause");
					system("cls");
				}
			}
			if (playCard == 2) 
			{
				if (sCards->cards == "Slave") 
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
					cout << endl;
					mm = mm * 100000;
					cout << "You won " << mm << " yen" << endl;
					cash = mm + mm;
					system("pause");
					system("cls");
				}
				else if (sCards->cards == "Civillian") 
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
					cout << endl;
					cout << "DRAW" << endl;
					emperorCards.pop_back();
					system("pause");
					system("cls");
					break;
				}
			}
			if (playCard == 3)
			{
				if (sCards->cards == "Slave")
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
					cout << endl;
					mm = mm * 100000;
					cout << "You won " << mm << " yen" << endl;
					cash = mm + mm;
					system("pause");
					system("cls");
				}
				else if (sCards->cards == "Civillian")
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
					cout << endl;
					cout << "DRAW" << endl;
					emperorCards.pop_back();
					system("pause");
					system("cls");
					break;
				}
			}
			if (playCard == 4)
			{
				if (sCards->cards == "Slave")
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
					cout << endl;
					mm = mm * 100000;
					cout << "You won " << mm << " yen" << endl;
					cash = mm + mm;
					system("pause");
					system("cls");
				}
				else if (sCards->cards == "Civillian")
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
					cout << endl;
					cout << "DRAW" << endl;
					emperorCards.pop_back();
					system("pause");
					system("cls");
					break;
				}
			}
			if (playCard == 5)
			{
				if (sCards->cards == "Slave")
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
					cout << endl;
					mm = mm * 100000;
					cout << "You won " << mm << " yen" << endl;
					cash = mm + mm;
					system("pause");
					system("cls");
				}
				else if (sCards->cards == "Civillian")
				{
					cout << "Open!" << endl;
					cout << endl;
					cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
					cout << endl;
					cout << "DRAW" << endl;
					emperorCards.pop_back();
					system("pause");
					system("cls");
					break;
				}
			}

			if (round == 4)
			{
				cout << "Pick a card to play..." << endl;
				cout << "======================" << endl;
				for (unsigned int i = 0; i < slaveCards.size(); i++) {
					cout << "[" << i + 1 << "] " << slaveCards[i] << endl;
				}
				cin >> playCard;

				emperorCards.push_back("Emperor");
				emperorCards.push_back("Civillian");
				emperorCards.push_back("Civillian");
				emperorCards.push_back("Civillian");
				emperorCards.push_back("Civillian");

				if (playCard == 1)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Slave vs [Tonegawa] Emperor" << endl;
						cout << endl;
						mm = mm * 100000;
						cout << "You won " << mm << " yen" << endl;
						cash = mm + mm;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Slave vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
						break;
					}
				}
				if (playCard == 2)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
						break;
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
				if (playCard == 3)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
				if (playCard == 4)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
				if (playCard == 5)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
			}
			if (round == 7)
			{
				cout << "Pick a card to play..." << endl;
				cout << "======================" << endl;
				for (unsigned int i = 0; i < emperorCards.size(); i++) {
					cout << "[" << i + 1 << "] " << emperorCards[i] << endl;
				}
				cin >> playCard;

				slaveCards.push_back("Slave");
				slaveCards.push_back("Civillian");
				slaveCards.push_back("Civillian");
				slaveCards.push_back("Civillian");
				slaveCards.push_back("Civillian");

				if (playCard == 1)
				{
					if (sCards->cards == "Slave")
					{
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
						break;
					}
					else if (sCards->cards == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Emperor vs [Tonegawa] Citizen" << endl;
						cout << endl;
						mm = mm * 100000;
						cout << "You won " << mm << " yen" << endl;
						cash = mm + mm;
						system("pause");
						system("cls");
					}
				}
				if (playCard == 2)
				{
					if (sCards->cards == "Slave")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
						cout << endl;
						mm = mm * 100000;
						cout << "You won " << mm << " yen" << endl;
						cash = mm + mm;
						system("pause");
						system("cls");
					}
					else if (sCards->cards == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						emperorCards.pop_back();
						system("pause");
						system("cls");
						break;
					}
				}
				if (playCard == 3)
				{
					if (sCards->cards == "Slave")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
						cout << endl;
						mm = mm * 100000;
						cout << "You won " << mm << " yen" << endl;
						cash = mm + mm;
						system("pause");
						system("cls");
					}
					else if (sCards->cards == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						emperorCards.pop_back();
						system("pause");
						system("cls");
						break;
					}
				}
				if (playCard == 4)
				{
					if (sCards->cards == "Slave")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
						cout << endl;
						mm = mm * 100000;
						cout << "You won " << mm << " yen" << endl;
						cash = mm + mm;
						system("pause");
						system("cls");
					}
					else if (sCards->cards == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						emperorCards.pop_back();
						system("pause");
						system("cls");
						break;
					}
				}
				if (playCard == 5)
				{
					if (sCards->cards == "Slave")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Slave" << endl;
						cout << endl;
						mm = mm * 100000;
						cout << "You won " << mm << " yen" << endl;
						cash = mm + mm;
						system("pause");
						system("cls");
					}
					else if (sCards->cards == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						emperorCards.pop_back();
						system("pause");
						system("cls");
						break;
					}
				}
			}
			if (round == 10)
			{
				cout << "Pick a card to play..." << endl;
				cout << "======================" << endl;
				for (unsigned int i = 0; i < slaveCards.size(); i++) {
					cout << "[" << i + 1 << "] " << slaveCards[i] << endl;
				}
				cin >> playCard;

				emperorCards.push_back("Emperor");
				emperorCards.push_back("Civillian");
				emperorCards.push_back("Civillian");
				emperorCards.push_back("Civillian");
				emperorCards.push_back("Civillian");

				if (playCard == 1)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Slave vs [Tonegawa] Emperor" << endl;
						cout << endl;
						mm = mm * 100000;
						cout << "You won " << mm << " yen" << endl;
						cash = mm + mm;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Slave vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
				}
				if (playCard == 2)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
				if (playCard == 3)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
				if (playCard == 4)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
				if (playCard == 5)
				{
					if (eCards->card == "Emperor")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Emperor" << endl;
						cout << endl;
						cout << "You lost! The pin will now move by 30mm" << endl;
						cout << "Drill....drill....drill...drill..." << endl;
						cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh..." << endl;
						system("pause");
						cout << "The pin has pierced your eardrum." << endl;
						cout << "No matter how much money you've won, it will not bring back what you've lost." << endl;
						cout << "Tonegawa will not be kneeling down on a loser like you." << endl;
						system("pause");
						system("cls");
					}
					else if (eCards->card == "Civillian")
					{
						cout << "Open!" << endl;
						cout << endl;
						cout << "[Kaiji] Civillian vs [Tonegawa] Civillian" << endl;
						cout << endl;
						cout << "DRAW" << endl;
						slaveCards.pop_back();
						system("pause");
						system("cls");
					}
				}
			}

		}
	}

}


int main()
{
	srand(time(NULL));
	vector<string>emperorCards;
	vector<string>slaveCards;

	printEmperor(emperorCards, slaveCards);



	return 0;
}