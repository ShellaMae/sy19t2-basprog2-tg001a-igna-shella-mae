// ConsoleApplication82.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include<iostream>
#include<string>
#include<time.h>
#include<vector>

using namespace std;

struct Item
{
	string name;
	int gold;
};

struct NPC
{
	string npcName;
	Item itemOrder;
};

NPC generateRandomNPC()
{
	int x = rand() % 4;
	NPC npc;
	switch (x)
	{
	case 1:
		npc.npcName = "Man";
		break;
	case 2:
		npc.npcName = "Old Man";
		break;
	case 3:
		npc.npcName = "Woman";
		break;
	case 4:
		npc.npcName = "Girl";
		break;
	default:
		break;
	}
	return npc;
	NPC Order;
	int y = rand() % 10 + 1;
	switch (y)
	{
	case 1:
		Order.itemOrder = { "Wooden Armband", 23 };
		break;
	case 2:
		Order.itemOrder = { "Leather Glove" ,	33 };
		break;
	case 3:
		Order.itemOrder = { "Wool Hat", 28 };
		break;
	case 4:
		Order.itemOrder = { "Focus Staff", 55 };
		break;
	case 5:
		Order.itemOrder = { "Wooden Shield", 62 };
		break;
	case 6:
		Order.itemOrder = { "Longsword", 122 };
		break;
	case 7:
		Order.itemOrder = { "Crafter's Knife", 272 };
		break;
	case 8:
		Order.itemOrder = { "Longbow", 186 };
		break;
	case 9:
		Order.itemOrder = { "Willpower Ring", 1083 };
		break;
	case 10:
		Order.itemOrder = { "Knight's Helm", 1596 };
		break;
	default:
		break;
		return Order;
	}
}

void Shop(vector<string> &database, Item ArrItem[10], int &Playergold)
{
	int ArrSellValue[10] = { 23, 33, 28, 55, 62, 122, 272, 186, 1083, 1596 };
	int randomItem = rand() % 10;
	int counter = 0;
	string Search[10];

	generateRandomNPC();

	cout << generateRandomNPC << " is looking for " << Search[counter];
	int x = 0;

	while (counter != 5)
	{

		for (int i = 0; i < database.size(); i++)
		{
			if (Search[i] == database.at(i))
			{
				cout << "Sold for " << Search[x] << " gold!" << endl;
				Playergold += ArrSellValue[0];
			}

			else if (database.at(i) != Search[i])
			{
				cout << "That item isn't in your shop, costumer left..." << endl;
			}
			system("pause");
			system("cls");
		}
		counter++;
	}
}

void printInventory(vector<string> &database, int &Playergold)
{
	cout << "Inventory items: " << endl;

	for (int i = 0; i < database.size(); i++)
	{
		cout << database.at(i) << endl;
	}
	cout << endl;
}

void guildShop(vector<string> &database, int &Playergold, Item ArrItem[10])
{
	string arrItem[10] = { "Wooden Armband", "Leather Glove", "Wool Hat", "Focus Staff", "Wooden Shield", "Longsword", "Crafter's Knife", "Longbow", "Willpower Ring", "Knight's Helm" };
	int arrValue[10] = { 15, 20, 25, 30, 35, 120, 140, 140, 600, 900 };

	for (int i = 0; i < 10; i++)
	{
		ArrItem[i] = { arrItem[i], arrValue[i] };
	}

	int input;
	cout << "Guild Items: " << endl;
	cout << "==============" << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << i + 1 << ") " << arrItem[i] << " | " << "Gold: " << arrValue[i] << endl;
	}
	cout << "Enter item to buy (0 to quit): ";
	cin >> input;
	if (input > 10)
	{
		cout << "Invalid choice!" << endl;
	}

	if (input == 0)
	{
		system("pause");
		system("cls");
		Shop(database, ArrItem, Playergold);
	}
	else if (arrValue[input - 1] > Playergold)
	{
		cout << "You don't have enough money to buy that item!" << endl;
	}
	else
	{
		database.push_back(arrItem[input - 1]);
		Playergold -= arrValue[input - 1];
		cout << arrItem[input - 1] << " bought!" << endl;
	}
	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));
	int playerGold = 1000;
	vector<string> database;
	Item ArrItem[10];
	while (true)
	{
		cout << "Current Gold: " << playerGold << endl;
		printInventory(database, playerGold);
		guildShop(database, playerGold, ArrItem);
		system("cls");
	}
	system("pause");
	return 0;
}

