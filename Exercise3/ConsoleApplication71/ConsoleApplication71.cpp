// ConsoleApplication71.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int betValue(int&gold, int&bet)
{
	gold = 1000;
	gold = gold - bet;

	return gold;
}

int diceRoll(int&roll)
{
	int dice1, dice2;
	dice1 = (rand() % 6 + 1);
	dice2 = (rand() % 6 + 1);
	roll = dice1 + dice2;

	return roll;
}

int payOut(int &gold, int aiRobot, int player, int bet)
{
	betValue(gold, bet);

	if (aiRobot == 2)
	{
		aiRobot = 13;
	}

	if (player == 2)
	{
		player = 13;
	}

	if (player > aiRobot)
	{
		if (player == 13)
		{
			cout << "SNAKE EYES!" << endl;
			gold = gold + (bet * 3);
		}
		gold = gold + (bet * 2);
	}

	else if (aiRobot > player)
	{
		if (aiRobot == 13)
		{
			cout << "SNAKE EYES!" << endl;
			gold = gold + (bet * 3);
		}
		gold = gold + (bet * 2);
	}

	else if (aiRobot == player)
	{
		cout << "DRAW!" << endl;
	}

	else if (aiRobot == 13 && player == 13)
	{
		cout << "SNAKE EYES!" << endl;
	}

	return gold;
}

int playRound()
{
	int aiRobot, player, bet;
	int gold = 1000;
	int roll = (rand() % 6 + 1) + (rand() % 6 + 1);
	betValue(gold, bet);
	aiRobot = diceRoll(roll);
	player = diceRoll(roll);

	cout << "How much will you bet: ";
	cin >> bet;
	gold = gold - bet;

	cout << "AI roll: " << roll << endl;
	aiRobot = roll;

	cout << "Player roll: " << roll << endl;
	player = roll;

	payOut(gold, aiRobot, player, bet);

	if (aiRobot == 2)
	{
		aiRobot = 13;
	}

	if (player == 2)
	{
		player = 13;
	}

	if (player > aiRobot)
	{
		if (player == 13)
		{
			cout << "SNAKE EYES!" << endl;
			gold = gold + (bet * 3);
		}
		gold = gold + (bet * 2);
	}

	else if (aiRobot > player) 
	{
		if (aiRobot == 13)
		{
			cout << "SNAKE EYES!" << endl;
			gold = gold + (bet * 3);
		}
		gold = gold + (bet * 2);

	}

	else if (aiRobot == player)
	{
		cout << "DRAW!" << endl;
	}

	else if (aiRobot == 13 && player == 13)
	{
		cout << "SNAKE EYES!" << endl;
	}

	if (gold = 0)
	{
		cout << "Player Loses!" << endl;
	}

	else if (gold != 0)
	{
		return roll;
	}

	return roll;
	
}

int main()
{
	srand(time(NULL));
	playRound();
	return 0;
}
